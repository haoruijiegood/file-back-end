/*
 Navicat Premium Data Transfer

 Source Server         : my aliyun
 Source Server Type    : MySQL
 Source Server Version : 110103
 Source Host           : 39.101.130.157:3306
 Source Schema         : my_file

 Target Server Type    : MySQL
 Target Server Version : 110103
 File Encoding         : 65001

 Date: 21/12/2023 10:52:05
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for t_file
-- ----------------------------
DROP TABLE IF EXISTS `t_file`;
CREATE TABLE `t_file` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `folder_id` bigint(20) DEFAULT NULL,
  `file_name` varchar(500) DEFAULT NULL,
  `storage_file_name` varchar(500) DEFAULT NULL,
  `type` varchar(500) DEFAULT NULL,
  `create_by` varchar(500) DEFAULT NULL,
  `create_time` date DEFAULT NULL,
  `file_url` varchar(500) DEFAULT NULL,
  `size` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Table structure for t_folder
-- ----------------------------
DROP TABLE IF EXISTS `t_folder`;
CREATE TABLE `t_folder` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `folder_name` varchar(500) DEFAULT NULL,
  `create_time` date DEFAULT NULL,
  `create_by` varchar(500) DEFAULT NULL,
  `type` varchar(500) DEFAULT NULL COMMENT '0文件夹 1文件',
  `file_url` varchar(500) DEFAULT NULL COMMENT '文件路径',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

SET FOREIGN_KEY_CHECKS = 1;
