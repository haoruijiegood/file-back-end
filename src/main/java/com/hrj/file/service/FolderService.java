package com.hrj.file.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.hrj.file.domain.po.FolderPo;
import com.hrj.file.domain.vo.FolderVo;

import java.util.List;

public interface FolderService extends IService<FolderPo> {
    /**
     * 查询文件属性
     */
    FolderVo queryById(Long id, int type);

    /**
     * 文件列表
     */
    List<FolderPo> list(FolderPo folderPo);

    //查询文件夹下的所有文件
    List<String> getStorageFileNameList(Long id);

}
