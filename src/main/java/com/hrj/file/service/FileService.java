package com.hrj.file.service;


import com.baomidou.mybatisplus.extension.service.IService;
import com.hrj.file.domain.po.FilePo;

import java.util.List;

public interface FileService extends IService<FilePo> {

    /**
     * 查询文件
     */
    List<FilePo> list(FilePo filePo);
}
