package com.hrj.file.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.hrj.file.domain.po.FilePo;
import com.hrj.file.mapper.FileMapper;
import com.hrj.file.service.FileService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.List;

@Service
public class FileServiceImpl extends ServiceImpl<FileMapper, FilePo> implements FileService {

    @Autowired
    private FileMapper fileMapper;

    /**
     * 查询文件
     */
    @Override
    public List<FilePo> list(FilePo filePo) {
        QueryWrapper<FilePo> wrapper = new QueryWrapper<>();
        wrapper.eq("folder_id",filePo.getFolderId());
        if (!StringUtils.isEmpty(filePo.getFileName())){
            wrapper.like("file_name",filePo.getFileName());
        }
        return fileMapper.selectList(wrapper);
    }
}
