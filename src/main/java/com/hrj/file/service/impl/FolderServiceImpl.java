package com.hrj.file.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.hrj.file.domain.po.FolderPo;
import com.hrj.file.domain.vo.FolderVo;
import com.hrj.file.mapper.FolderMapper;
import com.hrj.file.service.FolderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class FolderServiceImpl extends ServiceImpl<FolderMapper, FolderPo> implements FolderService  {

    @Autowired
    private FolderMapper folderMapper;

    /**
     * 查询文件属性
     */
    @Override
    public FolderVo queryById(Long id, int type) {
        return folderMapper.queryById(id, type);
    }

    /**
     * 文件列表
     */
    @Override
    public List<FolderPo> list(FolderPo folderPo) {
        return folderMapper.list(folderPo);
    }

    //查询文件夹下的所有文件
    @Override
    public List<String> getStorageFileNameList(Long id) {
        return folderMapper.getStorageFileNameList(id);
    }
}
