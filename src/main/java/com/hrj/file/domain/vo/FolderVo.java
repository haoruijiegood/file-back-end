package com.hrj.file.domain.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.util.Date;

@Data
public class FolderVo {

    private String folderName;

    @JsonFormat(pattern = "yyyy/MM/dd hh:mm:ss")
    private Date createTime;

    private String type;

    private String size;

    private Integer num;

}
