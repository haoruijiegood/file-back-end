package com.hrj.file.domain.vo;

import lombok.Data;
import org.springframework.util.StringUtils;

import java.util.HashMap;

/**
 * 响应vo
 */
public class ResponseObjectVo extends HashMap<String, Object> {
    private static final long serialVersionUID = 1L;

    /**
     * 状态码
     */
    public static final String CODE_TAG = "code";

    /**
     * 返回内容
     */
    public static final String MSG_TAG = "msg";

    /**
     * 数据对象
     */
    public static final String DATA_TAG = "data";

    /**
     * 初始化一个新创建的 ResponseObjectVo 对象，使其表示一个空消息。
     */
    public ResponseObjectVo() {
    }

    /**
     * 初始化一个新创建的 ResponseObjectVo 对象
     *
     * @param code 状态码
     * @param msg  返回内容
     */
    public ResponseObjectVo(int code, String msg) {
        super.put(CODE_TAG, code);
        super.put(MSG_TAG, msg);
    }

    /**
     * 初始化一个新创建的 ResponseObjectVo 对象
     *
     * @param code 状态码
     * @param msg  返回内容
     * @param data 数据对象
     */
    public ResponseObjectVo(int code, String msg, Object data) {
        super.put(CODE_TAG, code);
        super.put(MSG_TAG, msg);
        if (!StringUtils.isEmpty(data)) {
            super.put(DATA_TAG, data);
        }
    }

    /**
     * 返回成功消息
     *
     * @return 成功消息
     */
    public static ResponseObjectVo success() {
        return ResponseObjectVo.success("操作成功");
    }

    /**
     * 返回成功数据
     *
     * @return 成功消息
     */
    public static ResponseObjectVo success(Object data) {
        return ResponseObjectVo.success("操作成功", data);
    }

    /**
     * 返回成功消息
     *
     * @param msg 返回内容
     * @return 成功消息
     */
    public static ResponseObjectVo success(String msg) {
        return ResponseObjectVo.success(msg, null);
    }

    /**
     * 返回成功消息
     *
     * @param msg  返回内容
     * @param data 数据对象
     * @return 成功消息
     */
    public static ResponseObjectVo success(String msg, Object data) {
        return new ResponseObjectVo(200, msg, data);
    }

    /**
     * 返回错误消息
     *
     * @return
     */
    public static ResponseObjectVo error() {
        return ResponseObjectVo.error("操作失败");
    }

    /**
     * 返回错误消息
     *
     * @param msg 返回内容
     * @return 警告消息
     */
    public static ResponseObjectVo error(String msg) {
        return ResponseObjectVo.error(msg, null);
    }

    /**
     * 返回错误消息
     *
     * @param msg  返回内容
     * @param data 数据对象
     * @return 警告消息
     */
    public static ResponseObjectVo error(String msg, Object data) {
        return new ResponseObjectVo(500, msg, data);
    }

    /**
     * 返回错误消息
     *
     * @param code 状态码
     * @param msg  返回内容
     * @return 警告消息
     */
    public static ResponseObjectVo error(int code, String msg) {
        return new ResponseObjectVo(code, msg, null);
    }

    /**
     * 方便链式调用
     *
     * @param key   键
     * @param value 值
     * @return 数据对象
     */
    @Override
    public ResponseObjectVo put(String key, Object value) {
        super.put(key, value);
        return this;
    }

    /**
     * @author MZ
     * @date 2022/7/22
     * @description 返回警告信息
     **/
    public static ResponseObjectVo warning(String msg) {
        return new ResponseObjectVo(405,msg);
    }
}

