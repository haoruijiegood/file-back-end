package com.hrj.file.domain.po;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.util.Date;

/**
 * 文件夹
 */
@Data
@TableName("t_folder")
public class FolderPo {

    private Long id;

    private String folderName;

    @JsonFormat(pattern = "yyyy/MM/dd hh:mm:ss")
    private Date createTime;

    private String createBy;

    private Integer type;

    @TableField(exist = false)
    private String fileUrl;

}
