package com.hrj.file.domain.po;

import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.util.Date;

@Data
@TableName("t_file")
public class FilePo {

    private Long id;

    private Long folderId;

    private String fileName;

    //在对象存储中的名称
    private String storageFileName;

    private String type;

    private String createBy;

    @JsonFormat(pattern = "yyyy/MM/dd hh:mm:ss")
    private Date createTime;

    private String fileUrl;

    private String size;

}
