package com.hrj.file.task;

import com.hrj.file.util.MinioUtils;
import lombok.SneakyThrows;
import org.springframework.web.multipart.MultipartFile;

public class UploadTask extends Thread {


    private String bucketName;

    private MultipartFile multipartFile;

    private String code;


    public UploadTask(String bucketName, MultipartFile multipartFile, String code) {
        this.bucketName = bucketName;
        this.multipartFile = multipartFile;
        this.code = code;
    }

    @SneakyThrows
    @Override
    public void run() {
        MinioUtils.putObject(bucketName,multipartFile,code);
    }
}
