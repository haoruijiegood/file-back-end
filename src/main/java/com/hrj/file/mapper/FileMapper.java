package com.hrj.file.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.hrj.file.domain.po.FilePo;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface FileMapper extends BaseMapper<FilePo> {
}
