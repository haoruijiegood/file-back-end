package com.hrj.file.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.hrj.file.domain.po.FolderPo;
import com.hrj.file.domain.vo.FolderVo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface FolderMapper extends BaseMapper<FolderPo> {
    /**
     * 查询文件属性
     */
    FolderVo queryById(@Param("id") Long id, @Param("type") int type);

    /**
     * 文件列表
     */
    List<FolderPo> list(FolderPo folderPo);

    //查询文件夹下的所有文件
    List<String> getStorageFileNameList(Long id);
}
