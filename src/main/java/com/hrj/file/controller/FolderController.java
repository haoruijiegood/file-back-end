package com.hrj.file.controller;


import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.hrj.file.domain.po.FilePo;
import com.hrj.file.domain.po.FolderPo;
import com.hrj.file.domain.vo.FolderVo;
import com.hrj.file.domain.vo.ResponseObjectVo;
import com.hrj.file.service.FileService;
import com.hrj.file.service.FolderService;
import com.hrj.file.util.MinioUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;

/**
 * 文件夹
 */
@RestController
@CrossOrigin
@RequestMapping("/folder")
public class FolderController {

    @Autowired
    private FolderService folderService;

    @Autowired
    public FileService fileService;
    /**
     * 查询文件夹
     */
    @GetMapping("/list")
    public ResponseObjectVo list(FolderPo folderPo) {
        List<FolderPo> list = folderService.list(folderPo);
        return ResponseObjectVo.success(list);
    }

    /**
     * 查询文件属性
     */
    @GetMapping("/queryById/{id}/{type}")
    public ResponseObjectVo queryById(@PathVariable Long id, @PathVariable int type) {
        FolderVo folderVo = null;
        if (type == 1){
            folderVo = folderService.queryById(id,type);
        } else {
            folderVo = folderService.queryById(id,type);
            FolderPo folderPo = folderService.getById(id);
            folderVo.setFolderName(folderPo.getFolderName());
            folderVo.setCreateTime(folderPo.getCreateTime());
        }
        return ResponseObjectVo.success(folderVo);
    }

    /**
     * 创建文件夹
     */
    @PostMapping("/add")
    public ResponseObjectVo add(@RequestBody FolderPo folderPo) {
        folderPo.setCreateTime(new Date());
        boolean save = folderService.save(folderPo);
        return save == true ? ResponseObjectVo.success() : ResponseObjectVo.error();
    }


    /**
     * 重命名
     * @param id id
     * @param folderName 修改文件夹名称
     * @param type //类型 0 文件夹 1 文件
     */
    @GetMapping("/rename/{id}/{folderName}/{type}")
    @Transactional(rollbackFor = Exception.class)
    public ResponseObjectVo rename(@PathVariable Long id, @PathVariable String folderName, @PathVariable Integer type) {
        //修改文件夹
        folderService.update(new LambdaUpdateWrapper<FolderPo>()
                .eq(FolderPo::getId, id)
                .set(FolderPo::getFolderName, folderName));
        //如果只是一个文件则修改文件的名称
        if(type == 1){
            fileService.update(new LambdaUpdateWrapper<FilePo>()
                    .eq(FilePo::getFolderId,id)
                    .set(FilePo::getFileName,folderName));
        }
        return ResponseObjectVo.success();
    }

    @Value("${minio.fileBucket}")
    private String bucketName;
    /**
     * 删除
     */
    @GetMapping("/del/{id}/{type}")
    public ResponseObjectVo del(@PathVariable Long id, @PathVariable Integer type) {
        //查询文件夹下的所有文件
        List<String> list = folderService.getStorageFileNameList(id);
        list.forEach(l->{
            try {
                MinioUtils.removeObject(bucketName,l);
            } catch (Exception e) {
                throw new RuntimeException("删除文件夹失败，请重试!");
            }
        });
        //删除文件夹
        folderService.removeById(id);
        //如果只是一个文件则删除文件的名称
        fileService.remove(new LambdaQueryWrapper<FilePo>().eq(FilePo::getFolderId,id));
        return ResponseObjectVo.success();
    }
}
